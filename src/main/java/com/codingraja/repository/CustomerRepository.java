package com.codingraja.repository;

import java.util.List;

import com.codingraja.domain.Customer;

public interface CustomerRepository {
	public Customer save(Customer customer);
	public Customer findOne(Long id);
	public List<Customer> findAll();
	public void delete(Long id);
}
