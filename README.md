# Spring Boot RESTFul Web Services
[![N|Solid](http://www.innerrange.com/Portals/0/LayerSlider3D/606/redbutton_1_2_4.png)](http://codingraja.com/rest-webservices)

# Steps To Run this Application
  - Clone the application in your local machine
  - Import application in any IDE (Eclipse or IntelliJ Idea)
  - Open application.properties file and change your Database Details
  - Open Terminal and go to Application directory
   
  ```sh
    $ ./mvnw
  ```
  - To use this application install the RESTClient or Postman browser plugins.
   
   ```sh
    http://localhost:8080/api/customers
   ```

# Steps To Create Spring Boot Application

  - Create New Maven Project and Select Archetype- maven-archetype-quickstart 
  - Enter Group ID- com.codingraja.rest and Artifact ID - spring-boot-rest-webservices
  - Open your pom.xml file and add all required dependencies and plugins
###
* SpringBootRestApplication.java
* application.properties
* DataSourceConfig.java
* Customer.java
* CustomerRepository.java and CustomerRepositoryImpl.java
* CustomerService.java and CustomerServiceImpl.java
* CustomerResource.java

### To Run, Open Terminal and go to Application directory
   
  ```sh
    $ ./mvnw
  ```
  - To use this application install the RESTClient or Postman browser plugins.
   
   ```sh
    http://localhost:8080/api/customers
   ```

